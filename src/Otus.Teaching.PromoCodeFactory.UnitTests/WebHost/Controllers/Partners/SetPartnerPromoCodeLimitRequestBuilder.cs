﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Reflection;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    internal class SetPartnerPromoCodeLimitRequestBuilder
    {
        public DateTime EndDate { get; set; }

        public int Limit { get; set; }

        internal SetPartnerPromoCodeLimitRequestBuilder WithEndDate(DateTime endDate)
        {
            EndDate = endDate;

            return this;
        }

        internal SetPartnerPromoCodeLimitRequestBuilder WithLimit(int limit)
        {
            Limit = limit;

            return this;
        }

        internal SetPartnerPromoCodeLimitRequest Build()
        {
            return new SetPartnerPromoCodeLimitRequest
            {
                EndDate = EndDate, 
                Limit = Limit
            };
        }
    }
}
