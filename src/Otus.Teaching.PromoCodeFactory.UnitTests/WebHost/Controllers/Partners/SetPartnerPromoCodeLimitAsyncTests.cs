﻿using AutoFixture.AutoMoq;
using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using System.Collections.Generic;
using Xunit;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        /// <summary> 1. Если партнер не найден, то также нужно выдать ошибку 404 </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(5)
                .WithEndDate(DateTime.UtcNow.AddDays(10))
                .Build());

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary> 2. Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400; </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsActiveEqualsFalse_ReturnsBadRequest()
        {
            // Arrange
            var autoFixture = new Fixture();

            #region FSetup
            autoFixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList().ForEach(q => autoFixture.Behaviors.Remove(q));
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());
            #endregion

            var partner = autoFixture.Build<Partner>()
                .With(q => q.IsActive, false)
                .Create();
            _partnersRepositoryMock.Setup(repo => repo.GetAllAsync()).ReturnsAsync(new List<Partner>() { partner });

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(5)
                .WithEndDate(DateTime.UtcNow.AddDays(10))
                .Build());

            // Assert
            result.Should().Match(q => q is BadRequestResult || q is BadRequestObjectResult);
        }

        /// <summary>
        /// 3. Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, 
        /// если лимит закончился, то количество не обнуляется
        /// </summary>
        /// <remarks> Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes </remarks>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_WhenPreviousLimitIsNotEndsNumberIssuedPromoCodesAfterAction_ShouldBeZero()
        {
            // Arrange
            var autoFixture = new Fixture();

            #region FSetup
            autoFixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList().ForEach(q => autoFixture.Behaviors.Remove(q));
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());
            #endregion

            var limit = autoFixture.Build<PartnerPromoCodeLimit>()
                .With(q => q.EndDate, DateTime.Now.AddDays(1))
                .Without(q => q.CancelDate)
                .Create();

            var partner = autoFixture.Build<Partner>()
                .With(q => q.IsActive, true)
                .With(q => q.NumberIssuedPromoCodes, 10)
                .With(q => q.PartnerLimits, new List<PartnerPromoCodeLimit> { limit })
                .Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(5)
                .WithEndDate(DateTime.UtcNow.AddDays(10))
                .Build());

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        /// 3. Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, 
        /// если лимит закончился, то количество не обнуляется
        /// </summary>
        /// <remarks> Если лимит закончился, то количество не обнуляется </remarks>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_WhenPreviousLimitEndsNumberIssuedPromoCodesAfterAction_ShouldNotBeZero()
        {
            // Arrange
            var autoFixture = new Fixture();

            #region FSetup
            autoFixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList().ForEach(q => autoFixture.Behaviors.Remove(q));
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());
            #endregion

            var limit = autoFixture.Build<PartnerPromoCodeLimit>()
                .With(q => q.CreateDate, DateTime.Now.AddDays(-1))
                .With(q => q.EndDate, DateTime.Now.AddDays(-1))
                .Without(q => q.CancelDate)
                .Create();

            var partner = autoFixture.Build<Partner>()
                .With(q => q.IsActive, true)
                .With(q => q.NumberIssuedPromoCodes, 10)
                .With(q => q.PartnerLimits, new List<PartnerPromoCodeLimit> { limit })
                .Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(5)
                .WithEndDate(DateTime.UtcNow.AddDays(10))
                .Build());

            // Assert
            partner.NumberIssuedPromoCodes.Should().NotBe(0);
        }

        /// <summary> 4. При установке лимита нужно отключить предыдущий лимит; </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_CancelDateOfPreviousLimitsAfterAction_ShouldBeOnOrBeforeThenDateTimeNow()
        {
            // Arrange
            var autoFixture = new Fixture();

            #region FSetup
            autoFixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList().ForEach(q => autoFixture.Behaviors.Remove(q));
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());
            #endregion

            var limit = autoFixture.Build<PartnerPromoCodeLimit>()
                .Without(q => q.CancelDate)
                .Create();

            var partner = autoFixture.Build<Partner>()
                .With(q => q.IsActive, true)
                .With(q => q.PartnerLimits, new List<PartnerPromoCodeLimit> { limit })
                .Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            var newLimit = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(5)
                .WithEndDate(DateTime.UtcNow.AddDays(10))
                .Build();

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, newLimit);

            // Assert
            limit.CancelDate.Should().BeOnOrBefore(DateTime.Now);
        }

        /// <summary> 5. Лимит должен быть больше 0; </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerLimitIsZero_ReturnsBadRequest()
        {
            // Arrange
            var autoFixture = new Fixture();

            #region FSetup
            autoFixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList().ForEach(q => autoFixture.Behaviors.Remove(q));
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());
            #endregion
            var partner = autoFixture.Build<Partner>()
                .With(q => q.IsActive, true)
                .Create();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            var newLimit = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(0)
                .WithEndDate(DateTime.UtcNow.AddDays(10))
                .Build();
            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, newLimit);

            // Assert
            result.Should().Match(q => q is BadRequestResult || q is BadRequestObjectResult);
        }

        /// <summary> 6. Нужно убедиться, что сохранили новый лимит в базу данных (это нужно проверить Unit-тестом); </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_AfterSetLimitNewLimitInDatabase_IsNotNull()
        {
            // Arrange
            var serviceProvider = GetInMemoryServiceProvider();
            var controller = serviceProvider.GetRequiredService<PartnersController>();

            var autoFixture = new Fixture();

            #region FSetup
            autoFixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList().ForEach(q => autoFixture.Behaviors.Remove(q));
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());
            #endregion

            var partner = autoFixture.Build<Partner>()
                .Without(q => q.Id)
                .With(q => q.IsActive, true)
                .Create();

            var repository = serviceProvider.GetRequiredService<IRepository<Partner>>();
            await repository.AddAsync(partner);

            var limit = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(123)
                .WithEndDate(DateTime.UtcNow.AddDays(10))
                .Build();

            // Act
            var response = await controller.SetPartnerPromoCodeLimitAsync(partner.Id, limit);

            var createdAtActionResult = response as CreatedAtActionResult;
            var routeValues = createdAtActionResult.RouteValues;
            var partnerId = (Guid)routeValues.GetValueOrDefault("id");
            var limitId = (Guid)routeValues.GetValueOrDefault("limitId");

            var newLimit = (await repository.GetByIdAsync(partnerId)).PartnerLimits.FirstOrDefault(q => q.Id.Equals(limitId));

            // Assert
            newLimit.Should().NotBeNull();
        }

        private IServiceProvider GetInMemoryServiceProvider()
        {
            var builder = new ConfigurationBuilder();
            var configuration = builder.Build();
            var services = new ServiceCollection();
            services
               .AddSingleton(configuration)
               .AddSingleton((IConfiguration)configuration)
               .AddScoped<PartnersController>()
               .AddScoped(typeof(IRepository<>), typeof(EfRepository<>))
               .AddLogging(builder =>
               {
                   builder.ClearProviders();
                   builder.AddConfiguration(configuration.GetSection("Logging"));
                   builder
                       .AddFilter("Microsoft", LogLevel.Warning)
                       .AddFilter("System", LogLevel.Warning);
               })
               .AddMemoryCache();

            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            services.AddDbContext<DataContext>(options =>
            {
                options.UseInMemoryDatabase("InMemoryDb", builder => { });
                options.UseInternalServiceProvider(serviceProvider);
            });

            return services.BuildServiceProvider();
        }

    }
}